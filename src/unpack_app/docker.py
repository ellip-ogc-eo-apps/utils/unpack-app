import lxml.etree as etree
import yaml
import requests
import dpath
import os
import click
import sys 

def get_resource(url, api_key):
    
    r = requests.get(url,headers={'X-JFrog-Art-Api':api_key,
                                   'User-Agent': 'curl/t2Client'})

    return r


def get_process_version(ows_context):
    
    #parser = etree.XMLParser(remove_blank_text=True)
    #tree = etree.fromstring(ows_context.encode('utf-8'), parser)
    tree = ows_context
    xpath_expression = '/atom:feed/atom:entry/owc:offering[@code="{0}"]/owc:operation[@code="DescribeProcess"]/owc:result/atom:ProcessDescription'.format('http://www.opengis.net/spec/owc-atom/1.0/req/wps')            

    content = tree.xpath(xpath_expression,
                         namespaces={'atom':'http://www.w3.org/2005/Atom',
                                     'owc':'http://www.opengis.net/owc/1.0'})

    version = content[0].attrib['{http://www.opengis.net/wps/1.0.0}processVersion']
    
    return version
    
def app_descriptor(signature):

    version = '1.0'
    namespaces = dict()

    namespaces['xsi'] = 'http://www.w3.org/2001/XMLSchema-instance'
    
    for key, value in namespaces.items():
        etree.register_namespace(key, value)
   
    root = etree.Element('{{}}application'.format(namespaces['xsi']))
    root.attrib['id'] = 'application'
    #job_templates = etree.SubElement(root, '{{{}}}jobTemplates'.format(namespaces['xsi']))
    job_templates = etree.SubElement(root, 'jobTemplates')
    
    job_template = etree.SubElement(job_templates, 'jobTemplate')
    job_template.attrib['id'] = 'job'
    
    se = etree.SubElement(job_template, 'streamingExecutable')
    se.text = '/application/job/run.sh'
    
    df = etree.SubElement(job_template, 'defaultParameters')
    
    for key in signature.keys():
        
        if key in ['input_reference', 'input_reference_stack', 'service', 'data_path']:
            
            continue

        param = etree.SubElement(df, 'parameter')
        param.attrib['id'] = signature[key]['identifier']
        param.attrib['title'] = signature[key]['title']
        param.attrib['abstract'] = signature[key]['abstract']
        param.attrib['scope'] = 'runtime'
        param.attrib['type'] = 'LiteralData'
        
        if 'maxOccurs' in signature[key].keys():
            
            param.attrib['maxOccurs'] = signature[key]['maxOccurs']
           
        if 'minOccurs' in signature[key].keys():
            
            param.attrib['minOccurs'] = signature[key]['minOccurs']
        
        if 'options' in signature[key].keys():
            options = etree.SubElement(param, 'options')
            
            for option_value in signature[key]['options'].split(','):
                option = etree.SubElement(options, 'option')
                option.text = option_value
                
            default = etree.SubElement(options, 'default')
            default.text = signature[key]['value']
        else:
            
            param.text = signature[key]['value']
            
    djc = etree.SubElement(job_template, 'defaultJobconf')
    
    conf_prop = etree.SubElement(djc, 'property')
    conf_prop.attrib['id'] = 'mapred.task.timeout'
    conf_prop.text = '10800000'
    
    if 'input_reference_stack' in signature.keys():
        
        conf_prop = etree.SubElement(djc, 'property')
        conf_prop.attrib['id'] = 'ciop.job.max.tasks'
        conf_prop.text = '1'
    
    workflow = etree.SubElement(root, 'workflow')
    for key in ['id', 'title', 'abstract']:
        try:
            workflow.attrib[key] = signature['service'][key]
        except KeyError:
            workflow.attrib[key] = signature['service']['identifier'] 
   
    wv =  etree.SubElement(workflow, 'workflowVersion')
    wv.text = '1.0'
    
    node = etree.SubElement(workflow, 'node')
    node.attrib['id'] = 'node'
    
    job = etree.SubElement(node, 'job') 
    job.attrib['id'] = 'job'
    
    sources = etree.SubElement(node, 'sources') 

    source = etree.SubElement(sources, 'source') 
    
    source.attrib['refid'] = 'string:list'
    source.attrib['scope'] = 'runtime'
    
    if 'input_reference_stack' in signature.keys():
        for key in ['id', 'title', 'abstract']:
            try:
                source.attrib[key] = signature['input_reference_stack'][key]
            except KeyError:
                source.attrib[key] = signature['input_reference_stack']['identifier']    

        
        source.text = signature['input_reference_stack']['value']
        
    if 'input_reference' in signature.keys():
        for key in ['id', 'title', 'abstract']:
            try:
                source.attrib[key] = signature['input_reference'][key]
            except KeyError:
                source.attrib[key] = signature['input_reference']['identifier']    

        if 'value' in signature['input_reference'].keys():
            source.text = signature['input_reference']['value']
    
    return root #etree.tostring(root, pretty_print=True)


def get_docker(ows_context):
   
    tree = ows_context


    xpath_expression = '/atom:feed/atom:entry/owc:offering[@code="{0}"]/owc:content[@type="{1}"]'.format('http://www.opengis.net/spec/owc-atom/1.0/req/wps',                                                                                                      'application/vnd.docker.distribution.manifest.v1+json')

    content = tree.xpath(xpath_expression,
                         namespaces={'atom':'http://www.w3.org/2005/Atom',
                                     'owc':'http://www.opengis.net/owc/1.0'})

    docker_image = content[0].attrib['href']

        
    return docker_image
    
    
def get_cwl_reference(ows_context):
   
    tree = ows_context
    
    xpath_expression = '/atom:feed/atom:entry/owc:offering[@code="{0}"]/owc:content[@type="{1}"]'.format('http://www.opengis.net/spec/owc-atom/1.0/req/wps',                                                                                                      'application/cwl')

    content = tree.xpath(xpath_expression,
                         namespaces={'atom':'http://www.w3.org/2005/Atom',
                                     'owc':'http://www.opengis.net/owc/1.0'})

    cwl_url = content[0].attrib['href']
        
    return cwl_url
    
def get_signature(ows_context):
    
    #parser = etree.XMLParser(remove_blank_text=True)
    #tree = etree.fromstring(ows_context.encode('utf-8'), parser)
    tree = ows_context
    xpath_expression = '/atom:feed/atom:entry/owc:offering[@code="{0}"]/owc:operation[@code="DescribeProcess"]/owc:result/atom:ProcessDescription'.format('http://www.opengis.net/spec/owc-atom/1.0/req/wps')            

    content = tree.xpath(xpath_expression,
                         namespaces={'atom':'http://www.w3.org/2005/Atom',
                                     'owc':'http://www.opengis.net/owc/1.0'})

    process_description = content[0]
    
    signature = dict()
    
    service = dict()

    service['identifier'] = process_description.xpath('./ows:Identifier', 
                                                      namespaces={'atom':'http://www.w3.org/2005/Atom',
                                                                  'owc':'http://www.opengis.net/owc/1.0', 
                                                                  'ows':'http://www.opengis.net/ows/1.1'})[0].text

    service['title'] = process_description.xpath('./ows:Title', 
                                                 namespaces={'atom':'http://www.w3.org/2005/Atom',
                                                             'owc':'http://www.opengis.net/owc/1.0', 
                                                             'ows':'http://www.opengis.net/ows/1.1'})[0].text

    service['abstract'] = process_description.xpath('./ows:Abstract', 
                                                    namespaces={'atom':'http://www.w3.org/2005/Atom',
                                                                'owc':'http://www.opengis.net/owc/1.0', 
                                                                'ows':'http://www.opengis.net/ows/1.1'})[0].text
    
    signature['service'] = service
    
    
    # Input parameters
    input_parameters = process_description.xpath('./atom:DataInputs/wps:Input', 
                                                 namespaces={'atom':'http://www.w3.org/2005/Atom',
                                                             'owc':'http://www.opengis.net/owc/1.0', 
                                                             'wps':'http://www.opengis.net/wps/1.0.0'})
    
    
    for index, input_parameter in enumerate(input_parameters):
        
        param = dict()
        
        param['identifier'] = input_parameter.xpath('./ows:Identifier', 
                                                    namespaces={'atom':'http://www.w3.org/2005/Atom',
                                                                'owc':'http://www.opengis.net/owc/1.0', 
                                                                'ows':'http://www.opengis.net/ows/1.1'})[0].text

        param['title'] = input_parameter.xpath('./ows:Title', 
                                               namespaces={'atom':'http://www.w3.org/2005/Atom',
                                                           'owc':'http://www.opengis.net/owc/1.0', 
                                                           'ows':'http://www.opengis.net/ows/1.1'})[0].text

        param['abstract'] = input_parameter.xpath('./ows:Abstract', 
                                                  namespaces={'atom':'http://www.w3.org/2005/Atom',
                                                              'owc':'http://www.opengis.net/owc/1.0', 
                                                              'ows':'http://www.opengis.net/ows/1.1'})[0].text

        if 'minOccurs' in input_parameter.keys():

            param['min_occurs'] = input_parameter.attrib['minOccurs']

        if 'maxOccurs' in input_parameter.keys():

            param['max_occurs'] = input_parameter.attrib['maxOccurs']
            
        signature[param['identifier']] = param
        
        
    return signature

def get_parameters(ows_context):
    # get the list of parameters from the process description 
        
    #parser = etree.XMLParser(remove_blank_text=True)
    #tree = etree.fromstring(ows_context.encode('utf-8'), parser)
    tree = ows_context
    xpath_expression = '/atom:feed/atom:entry/owc:offering[@code="{0}"]/owc:operation[@code="DescribeProcess"]/owc:result/atom:ProcessDescription'.format('http://www.opengis.net/spec/owc-atom/1.0/req/wps')            

    content = tree.xpath(xpath_expression,
                         namespaces={'atom':'http://www.w3.org/2005/Atom',
                                     'owc':'http://www.opengis.net/owc/1.0'})

    process_description = content[0]
    
    # Input parameters
    input_parameters = process_description.xpath('./atom:DataInputs/wps:Input', namespaces={'atom':'http://www.w3.org/2005/Atom',
                                 'owc':'http://www.opengis.net/owc/1.0', 'wps':'http://www.opengis.net/wps/1.0.0'})
    
    
    parameters = dict()
    
    for index, input_parameter in enumerate(input_parameters):
        
        param = dict()
        
        param['identifier'] = input_parameter.xpath('./ows:Identifier', namespaces={'atom':'http://www.w3.org/2005/Atom',
                                     'owc':'http://www.opengis.net/owc/1.0', 
                                                                   'ows':'http://www.opengis.net/ows/1.1'})[0].text

        param['title'] = input_parameter.xpath('./ows:Title', namespaces={'atom':'http://www.w3.org/2005/Atom',
                                         'owc':'http://www.opengis.net/owc/1.0', 
                                                                       'ows':'http://www.opengis.net/ows/1.1'})[0].text

        param['abstract'] = input_parameter.xpath('./ows:Abstract', namespaces={'atom':'http://www.w3.org/2005/Atom',
                                         'owc':'http://www.opengis.net/owc/1.0', 
                                                                       'ows':'http://www.opengis.net/ows/1.1'})[0].text

        if 'minOccurs' in input_parameter.keys():

            param['min_occurs'] = input_parameter.attrib['minOccurs']

        if 'maxOccurs' in input_parameter.keys():

            param['max_occurs'] = input_parameter.attrib['maxOccurs']
            
        parameters[param['identifier']] = param
        
        
    return parameters


def get_cwl(ows_context, api_key):

    r = get_resource(get_cwl_reference(ows_context),
                 api_key)
    
    cwl = yaml.safe_load(r.content)
    
    return cwl

def is_scatter(cwl):
    
    for index, elem in enumerate(cwl['$graph']):

        if elem['id'] == 'node':

            node = elem

        if elem['id'] == 'main':

            main = elem
    
    is_scatter = False

    for index, elem in enumerate(dpath.util.get(main, 'requirements')):

        if (dpath.util.get(elem, 'class')) == 'ScatterFeatureRequirement':

            is_scatter = True

    return is_scatter

def get_scatter_parameter(cwl):
    
    if not is_scatter(cwl):
        
        raise NameError
        
    for index, elem in enumerate(cwl['$graph']):

        if elem['id'] == 'node':

            node = elem

        if elem['id'] == 'main':

            main = elem
    
    scatter = dpath.util.get(main, 'steps/step1/scatter')
    
    return dpath.util.get(main, 'steps/step1/in/{}'.format(scatter))


def get_cli(ows_context, cwl):

    for index, elem in enumerate(cwl['$graph']):

        if elem['id'] == 'node':

            node = elem

        if elem['id'] == 'main':

            main = elem

    cli = node['baseCommand']

    for key in node['inputs'].keys():
 
        if 'default' in node['inputs'][key].keys():

            #check if the value comes from the WPS
            if key in get_parameters(ows_context).keys():

                cli = (' '.join([cli, 
                             node['inputs'][key]['inputBinding']['prefix'], 
                             '$' + main['steps']['step1']['in'][key]]))
            else:

                cli = (' '.join([cli, 
                                 node['inputs'][key]['inputBinding']['prefix'], 
                                 node['inputs'][key]['default']]))

        else: 

            cli = (' '.join([cli, 
                             node['inputs'][key]['inputBinding']['prefix'], 
                             '$' + main['steps']['step1']['in'][key]]))

    return cli


def create_dyn_cwl(ows_context, api_key, params=False):
    
    cwl = get_cwl(ows_context, api_key)
    
    cwl_ref = get_cwl_reference(ows_context)
    
    app_cwl = os.path.basename(get_cwl_reference(ows_context))
    
    docker = get_docker(ows_context)
    
    scatter = is_scatter(cwl)
    
    if scatter: 
        scatter_parameter = get_scatter_parameter(cwl)
        
    parent_wf_params = dict()
    dyn_inputs = dict()
    step_in = dict()

    for index, elem in enumerate(cwl['$graph']):

        if elem['id'] == 'node':

            node = elem

        if elem['id'] == 'main':

            main = elem

    cli = node['baseCommand']

    for key in node['inputs'].keys():


        #check if the value comes from the WPS
        if main['steps']['step1']['in'][key] in get_parameters(ows_context).keys():

            if scatter and (main['steps']['step1']['in'][key] == scatter_parameter):

                parent_wf_params['_' + main['steps']['step1']['in'][key]] = ['$' + main['steps']['step1']['in'][key]]

                dyn_inputs['_' + main['steps']['step1']['in'][key]] = 'string[]'

                step_in[main['steps']['step1']['in'][key]] = '_' + main['steps']['step1']['in'][key]

            else:

                parent_wf_params['_' + main['steps']['step1']['in'][key]] = '$' + main['steps']['step1']['in'][key]
                dyn_inputs['_' + main['steps']['step1']['in'][key]] = 'string'
                step_in[main['steps']['step1']['in'][key]] = '_' + main['steps']['step1']['in'][key]

        else:

            if 'default' in node['inputs'][key].keys():

                #check if the value comes from the WPS
                if key in get_parameters(ows_context).keys():

                    parent_wf_params[main['steps']['step1']['in'][key]] = ''

                else:

                    parent_wf_params['_' + main['steps']['step1']['in'][key]] = node['inputs'][key]['default']

                step_in[main['steps']['step1']['in'][key]] = '_' + main['steps']['step1']['in'][key]
                dyn_inputs['_' + main['steps']['step1']['in'][key]] = 'string'

    if params:
        
        params_path = '_params.yaml'
        
        with open(params_path, 'w') as file:

            yaml.dump(parent_wf_params, file, default_flow_style=False)


    dyn_cwl = dict()

    dyn_cwl['class'] = 'Workflow'
    dyn_cwl['id'] = 'dynamic'
    dyn_cwl['inputs'] = dyn_inputs
 
    dyn_cwl['outputs'] = [{'id': 'dyout', 'outputSource': ['step1/outss'],'type': {'type': 'array',
                                  'items': {'items': 'File', 
                                            'type': 'array'}}}]
     
    dyn_cwl['hints'] = [{'class': 'SubworkflowFeatureRequirement'}, 
                        {'class': 'DockerRequirement', 
                         'dockerPull': docker}]

    dyn_cwl['steps'] = {'step1': {'in': step_in, 
                                  'out': ['outss'], 
                                  'run': app_cwl}}
    
    dyn_cwl['cwlVersion'] = 'v1.0'    

    cwl_path = '_wrapper.cwl'
    
    with open(cwl_path, 'w') as file:

        yaml.dump(dyn_cwl, file, default_flow_style=False)
        
        
def create_streaming_executable(ows_context, params_path, user, api_key):
    
    cwl = get_cwl(ows_context, api_key)

    scatter = is_scatter(cwl)

    if scatter: 
        scatter_parameter = get_scatter_parameter(cwl)

    cwl_path = './application/job/_wrapper.cwl'
        
    context = {'scatter_parameter': scatter_parameter,
               'dockerhub': get_docker(ows_context).split('/')[0], 
              'cwl_path': cwl_path, 
              'params_path': params_path}
        
    template = """#!/bin/bash

source ${{ciop_job_include}}


user=$( ciop-getparam _T2Username ) 
apikey=$( ciop-getparam _T2ApiKey ) 

echo "${{apikey}}" | docker login -u "${{user}}" --password-stdin {dockerhub}

""" 

    if scatter:
        
        template = template + """
while read {scatter_parameter}
do

    ciop-log 'INFO' 'Processing ${scatter_parameter}'  
    export {scatter_parameter}=${scatter_parameter}
    
    envsubst < {params_path} > __params.yaml
    
    /home/sysadmin/.conda/envs/cwl-validation/bin/cwl-runner {cwl_path} __params.yaml > results.out 2> results.err
    
done

cat results.out | jq -r \' .dyout | .[] | .[] .path\' | while read result
do

    ciop-log 'INFO' 'Publish ${{result}}'
    
    ciop-publish -m ${{result}}   

done"""
        
    else:
        
                template = template + """

input_reference_stack="`cat`"

input_reference_stack="$( echo $input_reference_stack | tr ' ' ',' )"

export input_reference_stack="$input_reference_stack"


envsubst < {params_path} > __params.yaml
    
/home/sysadmin/.conda/envs/cwl-validation/bin/cwl-runner {cwl_path} __params.yaml > results.out 2> results.err

rm -f __params.yaml

cat results.out | jq -r \' .dyout | .[] | .[] .path\' | while read result
do

    ciop-log 'INFO' 'Publish ${{result}}'
    
    ciop-publish -m ${{result}}   

done"""
            
    print(template.format(**context))
    

    
@click.command()
@click.option('--application_package', '-a')
@click.option('--username', '-u')
@click.option('--api_key', '-k')
@click.option('--version', 'command',
              flag_value='version',
              default=True)
@click.option('--descriptor', 'command',
              flag_value='descriptor')
@click.option('--streaming-exe', 'command',
              flag_value='streaming')
@click.option('--wrapper', 'command',
              flag_value='wrapper')
@click.option('--parameters', 'command',
              flag_value='parameters')
@click.option('--cwl-file', 'command',
              flag_value='cwlfile')
def main(application_package, username, api_key, command):
    
    r = get_resource(application_package,
                     api_key)
    
    ows_context = etree.fromstring(r.content)
    
    signature = get_signature(ows_context)
    
    param_username = dict([('identifier', '_T2Username'),
                 ('title', 'Username'),
                 ('abstract', 'Terradue username'),
                 ('value', ''), 
                 ('max_occurs', '1')])


    param_api_key = dict([('identifier', '_T2ApiKey'),
                    ('title', 'Terradue API key'),
                    ('abstract', 'Terradue API key'),
                    ('value', '')])
    
    
    signature['_T2Username'] = param_username
    signature['_T2ApiKey'] = param_api_key
    
    #descriptor = './application/application.xml'
    
    #streaming_excutable = './application/job/run.sh'
    
    #cwl_path = './application/job/_wrapper.cwl'
    
    #params_path = './application/job/_params.yaml'
     
    if command == 'descriptor':
        print('<?xml version="1.0" encoding="UTF-8"?>\n')
        print(etree.tostring(app_descriptor(signature), 
                             pretty_print=True).decode())
    
    if command == 'wrapper':
    
        print(get_cwl(ows_context, api_key))
        
    if command == 'version':
        
        print(get_process_version(ows_context))
        
    if command == 'streaming':
        
        create_streaming_executable(ows_context, params_path, username, api_key)
    
    if command == 'cwlfile':
        
        print(os.path.basename(get_cwl_reference(ows_context)))
        
        with open(os.path.basename(get_cwl_reference(ows_context)), 
                  'w') as file:

            yaml.dump(get_cwl(ows_context, api_key), 
                      file,
                      default_flow_style=False)

    if command == 'wrapper':

        create_dyn_cwl(ows_context, api_key)
        
    if command == 'parameters':

        create_dyn_cwl(ows_context, api_key, True)

    sys.exit(0)

if __name__ == '__main__':
   
    main()